package com.booking.models;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Reservation {
    private String reservationId;
    private Customer customer;
    private Employee employee;
    private List<Service> services;
    private double reservationPrice;
    private String workstage;
    //   workStage (In Process, Finish, Canceled)
    private static int reservationIdCounter = 1;

    public Reservation( Customer customer, Employee employee, List<Service> services,
            String workstage) {
        this.reservationId = generateUniqueRersevartionId();
        this.customer = customer;
        this.employee = employee;
        this.services = services;
        this.reservationPrice = calculateReservationPrice();
        this.workstage = workstage;
        incrementLoanIdCounter();
    };

    private static String generateUniqueRersevartionId() {
        String formattedCounter = String.format("%03d", reservationIdCounter);
        return "Rsv-" + formattedCounter;
    }
    private static void incrementLoanIdCounter() {
        reservationIdCounter++;
    }

    private double calculateReservationPrice(){
        double totalServicePrice = 0;
        double discount = 0;
        for (Service service : services) {
            totalServicePrice += service.getPrice();
        }

      
        if (customer != null && customer.getMember() != null && customer.getMember().getMembershipName() != null) {
            String membershipName = customer.getMember().getMembershipName();
            if ("Silver".equals(membershipName)) {
                discount = 0.05; 
            } else if ("Gold".equals(membershipName)) {
                discount = 0.10; 
            } else {
                discount = 0.0; 
            }
        }

        
        totalServicePrice = totalServicePrice - (totalServicePrice * discount);

     
        return totalServicePrice;
    }
    
}
