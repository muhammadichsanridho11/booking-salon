package com.booking.service;

import java.util.List;
import java.util.Scanner;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Service;

public class ValidationService {
    // Buatlah function sesuai dengan kebutuhan
    // public static void validateInput(){

    // }

   
    public static Customer validateCustomerId(List<Person> personList, String inputCustomerId, Scanner scanner) {
        Customer customer = ReservationService.getCustomerByCustomerId(personList, inputCustomerId);
        while (customer == null) {
            System.out.println("Cutomer yang di cari tidak tersedia");
            System.out.println("Silakan masukkan kembali Customer Id:");
            inputCustomerId = scanner.nextLine();
            customer = ReservationService.getCustomerByCustomerId(personList, inputCustomerId);
        }
        return customer;
    }


    public static Employee validateEmployeeById(List<Person> personList, String inputEmployeeId, Scanner scanner) {
        Employee employee = ReservationService.findEmployeeById(personList, inputEmployeeId);
        while (employee == null) {
            System.out.println("Employee yang di cari tidak tersedia");
            System.out.println("Silakan masukkan kembali Employee Id:");
            inputEmployeeId = scanner.nextLine();
            employee = ReservationService.findEmployeeById(personList, inputEmployeeId);
        }
        return employee;
    }
   
    public static Service validateServiceById(List<Service> services, String serviceId, List<Service> selectedServices) {
        for (Service service : services) {
            if (service.getServiceId().equals(serviceId)) {
                if (selectedServices.contains(service)) {
                    System.out.println("Service sudah dipilih.");
                    return null;
                } else {
                    return service;
                }
            }
        }
        System.out.println("Service yang dicari tidak tersedia.");
        return null;
    }
    

    

}
