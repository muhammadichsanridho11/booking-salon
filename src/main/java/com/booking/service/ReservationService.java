package com.booking.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class ReservationService {
    private static Scanner scanner = new Scanner(System.in);
    public static Customer getCustomerByCustomerId(List<Person> personList, String customerId) {
        for (Person person : personList) {
            if (person instanceof Customer && person.getId().equals(customerId)) {
                return (Customer) person;
            }
        }
        return null;
    } 
       
    public static void createReservation(List<Reservation> reservationList, List<Person> personList,List<Service> serviceList) {
    System.out.println("Membuat Reservasi\n");

    PrintService.showAllCustomer(personList);
  
    System.out.println("\nSilahkan Masukkan Customer Id:");
    String inputCustomerId = scanner.nextLine();
    Customer customer = ValidationService.validateCustomerId(personList, inputCustomerId, scanner);
    PrintService.showAllEmployee(personList);

    System.out.println("\nSilahkan Masukkan Employee Id");
    String employeeId = scanner.nextLine();
    Employee employee = ValidationService.validateEmployeeById(personList, employeeId, scanner);
    PrintService.showAllService(serviceList);
    
    List<Service> selectedServices = new ArrayList<>();
    String tambahanService = "Y"; 
    boolean continueSelecting = true;
    do {
        System.out.println("\nSilahkan Masukkan Service Id :");
        String serviceId = scanner.nextLine();
        if (serviceId.equalsIgnoreCase("T")) {
            continueSelecting = false; 
            break;
        }
        Service selectedService = ValidationService.validateServiceById(serviceList, serviceId, selectedServices);
        if (selectedService != null) {
            selectedServices.add(selectedService);
        } else {
            System.out.println("Service ID tidak valid.");
            continue; 
        }
        System.out.println("Ingin pilih service yang lain (Y/T)?");
        tambahanService = scanner.nextLine();
    } while (continueSelecting && tambahanService.equalsIgnoreCase("Y"));

    Reservation reservation = new Reservation(customer, employee, selectedServices,"In process");
    reservationList.add(reservation);
    System.out.println("Booking Berhasil!");
    System.out.println("Total Biaya Booking: Rp" + reservation.getReservationPrice());
    }

    public static void editReservationWorkstage(List<Reservation> reservationList){
        Scanner scanner = new Scanner(System.in);
        boolean isValidReservation = false;
        Reservation selectedReservation = null;
        while (!isValidReservation) {
            PrintService.showRecentReservation(reservationList);
            System.out.println("Silahkan Masukkan Reservation Id:");
            String inputReservationId = scanner.nextLine();
    
            selectedReservation = reservationList.stream()
                    .filter(reservation -> reservation.getReservationId().equals(inputReservationId))
                    .findFirst()
                    .orElse(null);
    
            if (selectedReservation == null) {
                System.out.println("Reservasi yang dicari tidak tersedia. Silakan coba lagi.");
            } else {
                isValidReservation = true;
            }
        }
            String inputStatus;
        boolean isValidStatus = false;

        do {
            System.out.println("Selesaikan reservasi (Finish/Cancel):");
            inputStatus = scanner.nextLine();

            if ("Finish".equals(inputStatus) || "Cancel".equals(inputStatus)) {
                isValidStatus = true;
            } else {
                System.out.println("Status yang dimasukkan tidak valid. Silakan masukkan 'Finish' atau 'Cancel'.");
            }
        } while (!isValidStatus);

        selectedReservation.setWorkstage(inputStatus);

        if ("Cancel".equals(inputStatus)) {
            selectedReservation.setReservationPrice(0);
        }

        System.out.println("Reservasi dengan id " + selectedReservation.getReservationId() + " sudah " + inputStatus);
    }
   
    public static Employee findEmployeeById(List<Person> personList, String employeeId) {
        Optional<Employee> employee = personList.stream()
            .filter(person -> person instanceof Employee)
            .map(person -> (Employee) person)
            .filter(person -> person.getId().equals(employeeId))
            .findFirst();

    return employee.orElse(null);
    }
}
